﻿<%@ Page Title="Database Design and Development" Language="C#" MasterPageFile="~/StudyGuide.Master" AutoEventWireup="true" CodeBehind="Databases.aspx.cs" Inherits="n01289618Assignment2a.SnippetDescription" %>

<%--Head--%>
<asp:Content ID="DatabaseContentHead" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<%--CSS--%>
<asp:Content ID="DatabaseCSS" ContentPlaceHolderID="css" runat="server">
        <link rel="stylesheet" href="/css/stylesheet.css" />
</asp:Content>

<%--Introduction--%>
<asp:Content ID="DatabaseIntroduction" ContentPlaceHolderID="ContentIntroduction" runat="server">
    <h1>Database Design and Development</h1>
    <p>
    Oh man, this is the course I feel I'm going to have the most trouble in. So writing queries
    is a little tricky for me. It doesn't come as quickly to me as C# or HTML/CSS does. Usually
    when I do the others, I can instantly think of the code I need to write out and write it. For
    Oracle, I have to think a little more about syntax and mess around until I eventually get the
    correct answer. Because of so, I figured I would dedicate a page towards it in this assignment. 
    I'm actually really excited! Let's get right into it. The requirements say I need a snippet of
    code that I wrote and to explain what it does. I'm going to take question 11 from the most recent
    assignment and use that.

    <h2>Assignment 4 Question 11: Database Design and Development</h2>
    <%--<img src="/img/SnippetDescription.png" class="img-fluid" id="DatabaseImage" alt="Responsive image">--%>
    </p>
    <CodeBox:CodeSnippet runat="server" CodeBlockTopic="Databases"  />
</asp:Content>

<%--What a join is--%>
<asp:Content ID="DatabaseJoin" ContentPlaceHolderID="ContentOverview" runat="server">
    <h3>Joins</h3>
    <p>
    So a concept we learned in class is joining tables together to retrieve data from multiple
    tables. This is useful in organization and is probably how actual searches work. Depending on what
    you type, I assume that it's taken and a query is written based on what you searched for. I am not 100%
    correct but I assume that is how Amazon works when you search for products on their site. 
    </p>
</asp:Content>

<%--How the query works--%>
<asp:Content ID="DatabaseQuery" ContentPlaceHolderID="ContentMain" runat="server">
    <h3>How does this query work?</h3>
    <p>
    So we want vendor names and line item descriptions. We can't get line item descriptions unless we
    join the table with that specific data. Therefore we use a left join because it says "if" they have invoices.
    If we used an inner join then the data set would all have invoices. Before this though, we have to join the invoices
    table as the vendors table does not have a key to join with the invoice line items table. After which, results 
    can be shown.
    </p>
</asp:Content>

<%--Moving Forward--%>
<asp:Content ID="DatabaseMovingForward" ContentPlaceHolderID="ContentSide" runat="server">
    <h3>Moving Forward</h3>
    <p>
    When aggregate functions get into the mix, things get even trickier. You'll be doing 
    joins, then have to add aggregate functions as well as sub queries. This is where my 
    brain really starts to have trouble. I suppose practice is key, but understanding what 
    to do also plays a big part also. I'll be looking into doing more example questions from
    previous labs and assignments. Maybe even aim to do the assignments without having to 
    reference anything at all. 
    </p>
</asp:Content>