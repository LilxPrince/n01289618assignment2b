﻿<%@ Page Title="New Concept" Language="C#" MasterPageFile="~/StudyGuide.Master" AutoEventWireup="true" CodeBehind="Angular2.aspx.cs" Inherits="n01289618Assignment2a.NewConcept" %>

<%--Head--%>
<asp:Content ID="Angular2ContentHead" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<%--CSS--%>
<asp:Content ID="Angular2CSS" ContentPlaceHolderID="css" runat="server">
        <link rel="stylesheet" href="/css/stylesheet.css" />
</asp:Content>

<%--Introduction--%>
<asp:Content ID="Angular2Introduction" ContentPlaceHolderID="ContentIntroduction" runat="server">
    <h1>Angular 2</h1>
    <p>
    So the understanding I wanted to solidify is how an Angular project is set up. 
    According to TutorialsPoint, Angular 2 is made up of "modules" that put logical
    boundaries into your application. There can be multiple modules and therefore 
    functionality can be separated and organized. This <a href="https://www.tutorialspoint.com/angular2/angular2_modules.htm">link</a> 
    is where I'm learning it from. Additionally, you can go to the official Angular <a href="https://angular.io/">site</a>
    to help with learning. 
    </p>
    <p>
    Disclaimer: All information on this page is referened and retrieved from Tutorials Point.
    Some of the information is in my own words, whereas some may be from the site. I do not claim
    ownership of these explanations being purely my own. 
    </p>
    <h2 id="moduleTitle">Module</h2>
    <%--<img src="/img/Module.png" class="img-fluid" id="Angular2Module" alt="Responsive image">--%>
    <CodeBox:CodeSnippet runat="server" CodeBlockTopic="AngularModule"/>
    <p id ="moduleExplanation">
    So this is what a module looks like. Let's break this down further. 
    </p>
    <ul>
        <li>The 3 import statements at the top are used to import:</li>
        <ul>
            <li>NgModule</li>
            <li>BrowserModule</li>
            <li>AppComponent</li>
        </ul>
        <li>NgModule is used to define imports, declarations and bootstrapping options</li>
        <li>BrowserModule is required for any web angular application</li>
        <li>AppComponent is required and contains code that renders view as well as supports it</li>
    </ul>
    <h2>Module parts</h2>
    <p>Modules are made up of 3 parts:</p>
</asp:Content>

<%--Bootstrap Array--%>
<asp:Content ID="Angular2BootstrapArray" ContentPlaceHolderID="ContentOverview" runat="server">
    <h3>Bootstrap Array</h3>
    <p>
    This tells Angular which components need to be loaded. The components loaded will provide
    functionality to the application. Once included, the component must be declared to enable
    use across other components in the application.
    </p>
</asp:Content>

<%--Export Array--%>
<asp:Content ID="Angular2ExportArray" ContentPlaceHolderID="ContentMain" runat="server">
    <h3>Export Array</h3>
    <p>This is used to export components, directives and pipes that can be used in other modules.</p>
</asp:Content>

<%--Import Array--%>
<asp:Content ID="Angular2ImportArray" ContentPlaceHolderID="ContentSide" runat="server">
    <h3>Import Array</h3>
    <p>This is used to import functionality from other modules</p>
</asp:Content>

<%--Components--%>
<asp:Content ID="Angular2Components" ContentPlaceHolderID="ContentIntroduction1" runat="server">
    <h2>Components</h2>
    <p>
    To go further into modules, they consist of multiple components. Components consist of templates,
    classes and metadata. Let's look into what that looks like. 
    </p>
    <CodeBox:CodeSnippet runat="server" CodeBlockTopic="AngularComponent" />
    <%--<img src="/img/Component.png" class="img-fluid" id="Angular2Component" alt="Responsive image">--%>
    <p id="componentExplanation">
    What's important is how the classes and templates work. The metadata refers to the lines of code
    with an @ symbol. The symbol declares the following element. 
    </p>
</asp:Content>

<%--Templates--%>
<asp:Content ID="Angular2Templates" ContentPlaceHolderID="ContentOverview1" runat="server">
    <h3>Templates</h3>
    <p>
    In the above code, the template is denoted by the "template" keyword and the HTML is written after
    the colon. The selector above it refers to the place in the index.html or page that you want the template
    code to go into. This means that 'my-app' has to be in the html file somewhere. Templates can be referenced
    and don't have to be inline such as in the example. To do so, you would write, "templateUrl: 'app/app.component.html'"
    or the directory where the template is kept. 
    </p>
</asp:Content>

<%--Classes--%>
<asp:Content ID="Angular2Classes" ContentPlaceHolderID="ContentMain1" runat="server">
    <h3>Classes</h3>
    <p>
    You can put C# code directly in the AppComponent and use integers, strings, you name it. In the example
    we have an object declared by notation, VariableName: VariableDataType = 'VariableValue'. The object can then be placed 
    straight into the template and referenced that way. 
    </p>
</asp:Content>

<%--Ending Graphic--%>
<asp:Content ID="Angular2EndingGraphic" ContentPlaceHolderID="ContentSide1" runat="server">
    <%--Image from Kanahei, a Line/Facebook sticker artist. Gotten from Google, rabbit's name is Usagi--%>
    <%--Code from Bootstrap: https://getbootstrap.com/docs/4.1/content/images/--%>    
    <img src="/img/thumbsup.png" class="img-fluid" id="thumbsup" alt="Responsive image">
</asp:Content>