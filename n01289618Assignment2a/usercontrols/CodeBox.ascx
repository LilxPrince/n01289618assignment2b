﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="n01289618Assignment2a.CodeBox" %>

<%--Set attributes of Angular Module code snippet--%>
<asp:DataGrid SkinID="dataGridSkin" ID="Angular_Module" runat="server" CssClass="code"
    GridLines="None" Width="500px" CellPadding="2">
</asp:DataGrid>

<asp:DataGrid SkinID="dataGridSkin" ID="Angular_Component" runat="server" CssClass="code"
    GridLines="None" Width="500px" CellPadding="2">
</asp:DataGrid>

<asp:DataGrid SkinID="dataGridSkin" ID="Databases" runat="server" CssClass="code"
    GridLines="None" Width="500px" CellPadding="2">
</asp:DataGrid>