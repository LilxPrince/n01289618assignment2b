﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace n01289618Assignment2a
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        private string codeTopic;
        public string CodeBlockTopic
        {
            get { return codeTopic; }
            set { codeTopic = value; }
        }

        //Code to create a code snippet
        DataView CodeSnippet()
        {
            //Instantiate a new table 
            DataTable codeBlock = new DataTable();

            //Make the first column that shows line numbers
            DataColumn idx_col = new DataColumn();

            //Make the second column that shows actual code
            DataColumn code_col = new DataColumn();

            //Make a row 
            DataRow codeRow;

            //Text for the first column, ColumnName is a pre-existing property
            idx_col.ColumnName = "Line";

            //Specify data type of the column
            idx_col.DataType = System.Type.GetType("System.Int32");
            
            //Use table class column function add to add column
            codeBlock.Columns.Add(idx_col);

            //Text for the second column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codeBlock.Columns.Add(code_col);

            //Code to output

            //Angular Module snippet
            List<string> Angular_Module_Code = new List<string>(new string[]
            {
                "import { NgModule } from '@angular/core';",
                "import { BrowserModule } from '@angular/platform-browser';",
                "import { AppComponent } from './app.component';",
                "@NgModule ({",
                "~imports: ~[ BrowserModule ],",
                "~declarations: ~[ AppComponent ],",
                "~bootstrap: ~[ AppComponent ]",
                "})",
                "export class AppModule { }",
            });

            //Angular Component snippet
            List<string> Angular_Component_Code = new List<string>(new string[]
            {
                "import { Component } from '@angular/core';",
                "@Component ({",
                "~selector: 'my-app',",
                "~template: `<div>",
                "~  <h1>{{appTitle}}</h1>",
                "~  <div>To Tutorials Point</div>",
                "~</div> `,",
                "})",
                "",
                "export class AppComponent {",
                "~appTitle: string = 'Welcome';",
                "}",
            });

            //Database Query snippet
            List<string> database_code = new List<string>(new string[]
            {
                "//11. Write a query that lists all the vendor names, and if they have invoices, list the line item description",
                "SELECT vendor_name, ili.line_item_descriptoin",
                "FROM vendors v",
                "JOIN invoices i",
                "ON i.vendor_id = i.vendor_id",
                "LEFT JOIN invoice_line_items ili",
                "ON ili.invoice_id = i.invoice_id",
            });

            switch(codeTopic)
            {
                case "AngularModule":
                    //Start at 0
                    int i = 0;
                    foreach (string line in Angular_Module_Code)
                    {
                        //For each line in the list, make a new row using NewRow method
                        codeRow = codeBlock.NewRow();
                        //Set the index column value to it's appropriate line number
                        codeRow[idx_col.ColumnName] = i;
                        //Set string to indicide what to replace in the list
                        string format_code = System.Net.WebUtility.HtmlEncode(line);
                        //If the code recognizes ~'s it will replace it with 3 spaces / indent
                        format_code = format_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                        codeRow[code_col.ColumnName] = format_code;
                        //Increment i
                        i++;
                        //Add the row of code
                        codeBlock.Rows.Add(codeRow);
                    }
                    break;

                case "AngularComponent":
                    //Start at 0
                    int j = 0;
                    foreach (string line in Angular_Component_Code)
                    {
                        //For each line in the list, make a new row using NewRow method
                        codeRow = codeBlock.NewRow();
                        //Set the index column value to it's appropriate line number
                        codeRow[idx_col.ColumnName] = j;
                        //Set string to indicide what to replace in the list
                        string format_code = System.Net.WebUtility.HtmlEncode(line);
                        //If the code recognizes ~'s it will replace it with 3 spaces / indent
                        format_code = format_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                        codeRow[code_col.ColumnName] = format_code;
                        //Increment i
                        j++;
                        //Add the row of code
                        codeBlock.Rows.Add(codeRow);
                    }
                    break;

                case "Databases":
                    //Start at 0
                    int k = 0;
                    foreach (string line in database_code)
                    {
                        //For each line in the list, make a new row using NewRow method
                        codeRow = codeBlock.NewRow();
                        //Set the index column value to it's appropriate line number
                        codeRow[idx_col.ColumnName] = k;
                        //Set string to indicide what to replace in the list
                        string format_code = System.Net.WebUtility.HtmlEncode(line);
                        //If the code recognizes ~'s it will replace it with 3 spaces / indent
                        format_code = format_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                        codeRow[code_col.ColumnName] = format_code;
                        //Increment i
                        k++;
                        //Add the row of code
                        codeBlock.Rows.Add(codeRow);
                    }
                    break;
            }
            
            //Switched from using an if statement to a switch above
            //if (codeTopic == "Angular")
            //{
            //    //Start at 0
            //    int i = 0;
            //    foreach (string line in back_end_code)
            //    {
            //        //For each line in the list, make a new row using NewRow method
            //        codeRow = codeBlock.NewRow();
            //        //Set the index column value to it's appropriate line number
            //        codeRow[idx_col.ColumnName] = i;
            //        //Set string to indicide what to replace in the list
            //        string format_code = System.Net.WebUtility.HtmlEncode(line);
            //        //If the code recognizes ~'s it will replace it with 3 spaces / indent
            //        format_code = format_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
            //        codeRow[code_col.ColumnName] = format_code;
            //        //Increment i
            //        i++;
            //        //Add the row of code
            //        codeBlock.Rows.Add(codeRow);
            //    }
            //}

            //else if (codeTopic == "Databases")
            //{
            //    //Start at 0
            //    int i = 0;
            //    foreach (string line in database_code)
            //    {
            //        //For each line in the list, make a new row using NewRow method
            //        codeRow = codeBlock.NewRow();
            //        //Set the index column value to it's appropriate line number
            //        codeRow[idx_col.ColumnName] = i;
            //        //Set string to indicide what to replace in the list
            //        string format_code = System.Net.WebUtility.HtmlEncode(line);
            //        //If the code recognizes ~'s it will replace it with 3 spaces / indent
            //        format_code = format_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
            //        codeRow[code_col.ColumnName] = format_code;
            //        //Increment i
            //        i++;
            //        //Add the row of code
            //        codeBlock.Rows.Add(codeRow);
            //    }
            //}

            //Create a view of the table and return it to the method
            DataView codeView = new DataView(codeBlock);
            return codeView;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CodeSnippet();

            switch(codeTopic)
            {
                case "AngularModule":
                    Angular_Module.DataSource = ds;
                    Angular_Module.DataBind();
                    break;
                case "AngularComponent":
                    Angular_Component.DataSource = ds;
                    Angular_Component.DataBind();
                    break;
                case "Databases":
                    Databases.DataSource = ds;
                    Databases.DataBind();
                    break;
            }

            //Switched from using an if statement to a switch above
            //if (codeTopic == "AngularModule")
            //{
            //    Angular_Module.DataSource = ds;
            //    Angular_Module.DataBind();
            //}

            //else if (codeTopic == "Databases")
            //{
            //    Databases.DataSource = ds;
            //    Databases.DataBind();
            //}

        }
    }
}