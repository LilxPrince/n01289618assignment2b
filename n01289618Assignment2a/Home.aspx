﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudyGuide.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="n01289618Assignment2a.Home" %>

<%--Head--%>
<asp:Content ID="HomeContentHead" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<%--CSS--%>
<asp:Content ID="HomeCSS" ContentPlaceHolderID="css" runat="server">
        <link rel="stylesheet" href="/css/stylesheet.css" />
</asp:Content>

<%--Home--%>
<asp:Content ID="HomeOverview" ContentPlaceHolderID="ContentIntroduction" runat="server">
    <h1>Jing's Study Guide Overview</h1>
    <p id ="homeIntroduction">
    Welcome to my study guide! Here, I'm going to go over some key concepts related to my courses.
    The focus will be on object oriented concepts such as classes and objects as well as
    Oracle database queries. Additionally, I'm looking into Angular 2 on my free time. It looks really
    interesting, but a tad confusing. I'll have a section of the site dedicated to Angular concepts that
    I wish to fully understand. There will also be code examples/snippets.
    </p>
    <h2>Assignment Requirements</h2>
    <p id="paragraph1">
    For the sake of the assignment, I will re-iterate the two main requirements and where my assignment
    meets them.
    </p>
    <ul>
        <li class="listnumbers">A master page with 4 content placeholder server controls</li>
            <ul class="listitems">
                <li class="requirements">ContentIntroduction ContentPlaceHolder (StudyGuide.Master Line 34)</li>
                <li class="requirements">ContentOverview ContentPlaceHolder (StudyGuide.Master Line 40)</li>
                <li class="requirements">ContentMain ContentPlaceHolder (StudyGuide.Master Line 46)</li>
                <li class="requirements">ContentSide ContentPlaceHolder (StudyGuide.Master Line 52)</li>
            </ul>
        <li class="listnumbers">3 Content pages which reference the master page</li>
            <ul class="listitems">
                <li class="requirements">Home.aspx</li>
                <li class="requirements">OOP.aspx</li>
                <li class="requirements">Databases.aspx</li>
                <li class="requirements">Angular2.aspx</li>
            </ul>
    </ul>
    <%--Image from Kanahei, a Line/Facebook sticker artist. Gotten from Google, rabbit's name is Usagi--%>
    <%--Code from Bootstrap: https://getbootstrap.com/docs/4.1/content/images/--%>
    <img src="/img/hi.gif" class="img-fluid" id="homeImage" alt="Responsive image">
</asp:Content>

<%--OOP--%>
<asp:Content ID="HomeOOP" ContentPlaceHolderID="ContentOverview" runat="server">
    <h3>Object Oriented Programming</h3>
    <p>
    In this page, I will be going over classes as well as objects. I think these two are very important
    concepts to know! This section may also touch on inheritance learned in the class before master pages. 
    </p>
</asp:Content>

<%--Databases--%>
<asp:Content ID="HomeDatabases" ContentPlaceHolderID="ContentMain" runat="server">
    <h3>Databases</h3>
    <p>
    A course I'm concerned with is Database Design and Development. Being new to writing
    queries and using Oracle, I feel that I need a bit more practice to be comfortable with it.
    This page will be dedicated to strengthening my knowledge in writing queries!
    </p>
</asp:Content>

<%--Angular2--%>
<asp:Content ID="HomeAngular2" ContentPlaceHolderID="ContentSide" runat="server">
    <h3>Angular 2</h3>
    <p>
    Recently in my free time, I started putting time into independent study. I've chosen
    to learn Angular 2 and I aim to understand it as well as create a project with it by the
    end of this year. This section will be explaining concepts to help me along my 
    journey.
    </p>
</asp:Content>