﻿<%@ Page Title="Object Oriented Concepts" Language="C#" MasterPageFile="~/StudyGuide.Master" AutoEventWireup="true" CodeBehind="OOP.aspx.cs" Inherits="n01289618Assignment2a.TrickyConcept" %>

<%--Head--%>
<asp:Content ID="OOPContentHead" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<%--CSS--%>
<asp:Content ID="OOPCSS" ContentPlaceHolderID="css" runat="server">
        <link rel="stylesheet" href="/css/stylesheet.css" />
</asp:Content>

<%--OOP Introduction--%>
<asp:Content ID="OOPIntroduction" ContentPlaceHolderID="ContentIntroduction" runat="server">
    <h1>Object Oriented Programming</h1>
    <p>
    In our ASP.NET course with Christine Bittle, we've jumped into object oriented concepts using C#.
    I want to go over something very important that we went over in week 4: classes, objects and methods. 
    These are key concepts in organizing and writing efficient code. Let's jump right into it!
        
    </p>
</asp:Content>

<%--Classes Overview--%>
<asp:Content ID="OOPOverview" ContentPlaceHolderID="ContentOverview" runat="server">
    <h3>Classes</h3>
    <p>
    So to start off, let's go over what a class is in C#. A class is essentially an outline for an object.
    For example, we can make a class for a monster in a game. This "monster" needs certain properties to make it
    come to life. We need its health, name, type, size and so on! After we define the class, we can then declare it
    and make an object. I'll be explaining this below. The example declaration assumes that the parameters used are already
    declared and initialized.
    </p>
</asp:Content>

<%--Declaring a class--%>
<asp:Content ID="OOPDeclaration" ContentPlaceHolderID="ContentMain" runat="server">
<%--I know using breaks is bad coding practice as we can handle it in the css, but for sake of
    time I'll do it this way. Will be sure to use actual pictures for code snippets in other pages.~--%>
    <h3>Declaring a class</h3>
    <p>
    Here's an example for defining a class.
    <br />
    public class Monster <br />
    { <br />
        public string name; <br />
        public int health; <br />
        public string type; <br />
        public char size; <br />
        public int attack;
        <br />
        public Monster(string n, int h, string t, char s, int a) <br />
        {<br />
        name = n;<br />
        health = h;<br />
        type = t;<br />
        size = s;<br />
        attack = a;<br />
        }<br />

    }
    </p>
</asp:Content>

<%--Initializing a class--%>
<asp:Content ID="OOPInitialization" ContentPlaceHolderID="ContentSide" runat="server">
    <h3>Iniitializing a class</h3>
    <p>
    Monster azureRathalos = new Monster(monsterName, monsterHealth, monsterType, monsterSize, monsterAttack);
    </p>
    <%--Image from Kanahei, a Line/Facebook sticker artist. Gotten from Google, rabbit's name is Usagi--%>
    <%--Code from Bootstrap: https://getbootstrap.com/docs/4.1/content/images/--%>    
    <img src="/img/hmm.png" class="img-fluid" id="OOPImage" alt="Responsive image">
</asp:Content>

<%--Object Introduction--%>
<asp:Content ID="OOPObject" ContentPlaceHolderID="ContentOverview1" runat="server">
    <h3>Objects</h3>
    <p>
    Objects are entities in memory that are configured based on a class.
    After created, they can be used to do neat stuff based on what the class is.
    For example, you can add a hitbox class to the namespace as well as an object loader 
    and actually use it in a game. 
    </p>
</asp:Content>

<%--Using the object after class declaration--%>
<asp:Content ID="OOPObjectUse" ContentPlaceHolderID="ContentMain1" runat="server">
    <h3>Using an object</h3>
    <p>
    To use an object, we can reference the name and target a specific property.<br />
    azureRathalos.monsterHealth -= pinkRathian.monsterAttack; <br />
    Assuming we have another monster instantiated, we can have them fight and calculate
    HP this way.
    </p>
</asp:Content>

<%--Words of Encouragement--%>
<asp:Content ID="OOPWoE" ContentPlaceHolderID="ContentSide1" runat="server">
    <h3>Hope you're still following along!</h3>
    <p>
    That was quite alot to think about. Don't get it? I don't blame you. 
    These concepts are definitely complicated to new programmers, but after a while
    it turns into second nature. Keep at it and you'll be fine. Be sure to click the "Databases" button
    on the menu to go to the next chapter. See you there!
    </p>
</asp:Content>
